import { sqliteTable, text, integer } from 'drizzle-orm/sqlite-core'

export const todos = sqliteTable('todos', {
  id: text('id').primaryKey(),
  userId: integer('user_id').notNull(), // GitHub Id
  title: text('title').notNull(),
  completed: integer('completed').notNull().default(0),
  createdAt: integer('created_at', { mode: 'timestamp' }).notNull(),
  instanceID: text('instance_id').notNull(),
  name: text('name').notNull(),
  instanceType: text('instance_type'),
  isTemplate: integer('is_template').notNull().default(0),
  templateID: text('template_id'),
})
