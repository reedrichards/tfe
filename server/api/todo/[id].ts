import { eq, and } from 'drizzle-orm'
import { useValidatedParams, z, } from 'h3-zod'
import config from '../../../config'

export default eventHandler(async (event) => {
  const { id } = await useValidatedParams(event, {
    id: z.string().uuid()
  })
  console.log("id", id) 

  const session = await requireUserSession(event)

  // List todos for the current user
  const todo = await useDB().select()
    .from(tables.todos)
    .where(
      and(
        eq(tables.todos.id, id),
        eq(tables.todos.userId, session.user.id)
      )).get()

  console.log("todo:", todo)
    

  let r;
  try {
    r = await fetch(`${config.BACKEND_URL}/flake/${todo?.instanceID}`,{
      headers: {
        'X-Token': process.env.AUTH_TOKEN??"",
      },
    })
  }
  catch (e) {
    console.log(e)
  }
  console.log("r", r)
  let json = await r?.json()
  console.log("json", json)


  const deployments = await useDB().select()
    .from(tables.todos)
    .where(
      and(
        eq(tables.todos.templateID, id),
        eq(tables.todos.userId, session.user.id)
      )).all()
  console.log("deployments", deployments)
  // deployments [
  //   {
  //     id: 'b34c5571-924f-4a5b-903d-4c2698462638',
  //     userId: 57335981,
  //     title: 'gitlab:reedrichards/f#default',
  //     completed: 0,
  //     createdAt: 2024-02-15T18:54:16.000Z,
  //     instanceID: '1285ed42-f881-442e-b2aa-bd869d643125',
  //     name: 'fleet-yak',
  //     instanceType: null,
  //     isTemplate: 0,
  //     templateID: '8910e326-9bec-4bfa-af31-087c6155730d'
  //   }
  // ]
  const djson = await  Promise.all(deployments.map(async (d) => {
    let r;
    try {
      r = await fetch(`${config.BACKEND_URL}/flake/${d.instanceID}`,{
        headers: {
          'X-Token': process.env.AUTH_TOKEN??"",
        },
      })
    }
    catch (e) {
      console.log(e)
    }
    return await r?.json()
  }));
  console.log("djson", djson)

  return {
    todo,
    json,
    deployments,
    djson,
  }
})