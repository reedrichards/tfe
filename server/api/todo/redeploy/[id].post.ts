import { useValidatedParams, useValidatedBody, z, } from 'h3-zod'
import { v4 as uuidv4 } from 'uuid';
import petname from 'node-petname'
import { eq } from 'drizzle-orm'
import config from '../../../../config'


export default eventHandler(async (event) => {

  const { id } = await useValidatedParams(event, {
    id: z.string().uuid()
  })
  const session = await requireUserSession(event)

  // get todo from database to get its instanceID
  let t = await (await useDB().select().from(tables.todos).where(eq(tables.todos.id, id)))
  console.log("t", t)

  



  let r = await fetch(`${config.BACKEND_URL}/flake/redeploy/${t[0].instanceID}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      'X-Token': process.env.AUTH_TOKEN ?? "",
    },
    body: JSON.stringify({
      flakeURL: t[0].title,
      instanceType: t[0].instanceType,
      template: t[0].instanceID,
    }),
  })


  let name = petname(2, "-")

  let json = await r?.json()
  console.log("json", json)
  let title = json.flakeCompute.flakeURL
  console.log("title", title)
  let instanceID = json.flakeCompute.id


  // List todos for the current user
  const todo = await useDB().insert(tables.todos).values({
    id: uuidv4(),
    userId: session.user.id,
    title: title,
    createdAt: new Date(),
    instanceID,
    name,
    instanceType: t[0].instanceType,
    templateID: id,
  }).returning().get()

  return todo

})