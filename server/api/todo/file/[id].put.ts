import { useValidatedParams, useValidatedBody, z, } from 'h3-zod'
import config from '../../../../config'

export default eventHandler(async (event) => {

  console.log("event", event)
  const { id } = await useValidatedParams(event, {
    id: z.string().uuid()
  })

  const { files } = await useValidatedBody(event, {
    files: z.array(z.object({
      path: z.string(),
      content: z.string(),
      id: z.string().uuid(),
    }))
  })
  const session = await requireUserSession(event)

  console.log("putting files", files)

  let r;
  try {
    r = await fetch(`${config.BACKEND_URL}/flake/${id}/files`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        'X-Token': process.env.AUTH_TOKEN ?? "",
      },
      body: JSON.stringify(files)
    })
  }
  catch (e) {
    console.log(e)
  }

  return

})