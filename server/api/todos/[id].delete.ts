import { eq, and } from 'drizzle-orm'
import { useValidatedParams, z } from 'h3-zod'
import config from '../../../config';

export default eventHandler(async (event) => {
  const { id } = await useValidatedParams(event, {
    id: z.string().uuid()
  })
  const session = await requireUserSession(event)

  const todo = await useDB().select()
  .from(tables.todos)
  .where(
    and(
      eq(tables.todos.id, id),
      eq(tables.todos.userId, session.user.id)
    )).get()

  console.log("todo:", todo)

  // List todos for the current user
  const deletedTodo = await useDB().delete(tables.todos).where(and(
    eq(tables.todos.id, id),
    eq(tables.todos.userId, session.user.id)
  )).returning().get()
  
  if (!deletedTodo) {
    throw createError({
      statusCode: 404,
      message: 'Todo not found'
    })
  }

  let url =`${config.BACKEND_URL}/flake/${todo?.instanceID}`

  // fetch url with DELETE
  try {
    await fetch(url, {
      method: "DELETE",
      headers: {
        'X-Token': process.env.AUTH_TOKEN??"",
      },
    })
  }
  catch (e) {
    console.log(e)
  }



  return deletedTodo
})