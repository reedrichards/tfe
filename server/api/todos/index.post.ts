import { useValidatedBody, z } from 'h3-zod'
import { v4 as uuidv4 } from 'uuid';
import petname from 'node-petname'
import config from '../../../config'

export default eventHandler(async (event) => {
  const { title, files, isTemplate } = await useValidatedBody(event, {
    title: z.string(),
    files: z.array(z.object({
        path: z.string(),
        content: z.string(),
    })),
    isTemplate: z.boolean(),
  })
  let name = petname(2, "-")

  console.log("title", title)
  const session = await requireUserSession(event)
  let r;

  try {
    r = await fetch(`${config.BACKEND_URL}/flake`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        'X-Token': process.env.AUTH_TOKEN??"",
      },
      body: JSON.stringify({
        flakeURL: title,
        files: files,
        isTemplate: isTemplate,
      })
    })
  }
  catch (e) {
    console.log(e)
  }

  console.log("r", r) 


  let json = await r?.json()

  console.log("json", json)

  let instanceID = json.flakeCompute.id
  

  // List todos for the current user
  const todo = await useDB().insert(tables.todos).values({
    id: uuidv4(),
    userId: session.user.id,
    title,
    createdAt: new Date(),
    instanceID: instanceID,
    name,
    isTemplate: isTemplate ? 1 : 0,

  }).returning().get()

  return todo
})