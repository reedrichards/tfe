import { eq, and } from 'drizzle-orm'
import { useValidatedParams, useValidatedBody, z,  } from 'h3-zod'

export default eventHandler(async (event) => {
  const { id } = await useValidatedParams(event, {
    id: z.string().uuid()
  })
  const { name, title, instanceType } = await useValidatedBody(event, {
    name: z.string(),
    title: z.string(),  
    instanceType: z.string().nullable(),
  })
  const session = await requireUserSession(event)

  // List todos for the current user
  console.log('patching flake')
  const todo = await useDB().update(tables.todos).set({
    name, title, instanceType
  }).where(and(
    eq(tables.todos.id, id),
    eq(tables.todos.userId, session.user.id)
  )).returning().get()
  console.log("patched", todo) 
  
  return todo
})