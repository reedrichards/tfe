{ pkgs, ...
}:


pkgs.mkShell {
  hardeningDisable = [ "fortify" ];

  packages = [
    pkgs.nodePackages_latest.pnpm
    pkgs.nodejs
    pkgs.nodePackages_latest.wrangler
  ];
}
    