import { readonly, ref } from 'vue';

export function useReducer(reducer: (arg0: any, arg1: any) => any, initialArg: any, init: (arg0: any) => any) {
  const state = ref(init ? init(initialArg) : initialArg);
  const dispatch = (action: any) => {
    state.value = reducer(state.value, action);
  };

  return [state, dispatch];
};